<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Spatie\Activitylog\LogOptions;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Report extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, LogsActivity;

    protected $guarded = [];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
                ->logOnly(['title', 'description', 'status'])
                ->setDescriptionForEvent(fn(string $eventName) => "This model has been {$eventName}")
                ->useLogName('Report');
    }

    protected $fillable = [
        'reporter_id',
        'category_id',
        'ticket_id',
        'title',
        'description',
        'status',
    ];

    public function Reporter()
    {
        return $this->belongsTo(Reporter::class);
    }

    public function Category()
    {
        return $this->belongsTo(Category::class);
    }

    public function ReportTrackers()
    {
        return $this->hasMany(ReportTracker::class);
    }
}
