<?php

namespace App\Http\Controllers\Admin;

use App\Models\Report;
use App\Models\Category;
use App\Models\Reporter;
use Illuminate\Http\Request;
use App\Models\ReportTracker;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = Report::join('reporters', 'reports.reporter_id', '=', 'reporters.id')->select('reports.*', 'reporters.*');
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($item) {
                $button =
                    '<div class="d-flex gap-2">
                        <a href="'.route('admin.report.log', $item->id).'" class="btn btn-primary">Log</a>
                        <a href="'.route('admin.report.show', $item->id).'" class="btn btn-warning">Show</a>
                        <a href="'.route('admin.report.edit', $item->id).'" class="btn btn-secondary">Edit</a>
                        <form class="inline-block" action="'. route('admin.report.destroy', $item->id) .'" method="POST">
                            <button class="btn btn-danger" >
                                Delete
                            </button>
                        '. method_field('delete') . csrf_field() .'
                        </form>
                    </div>';
                    //'<div class="dropdown">
                    //    <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    //        Action
                    //    </button>
                    //    <ul class="dropdown-menu">
                    //        <li><a class="dropdown-item" href="'.route('admin.report.show', $item->id).'">Edit</a></li>
                    //        <li><a class="dropdown-item" href="'.route('admin.report.edit', $item->id).'">Edit</a></li>
                    //        <li><a class="dropdown-item" href="#">Delete</a></li>
                    //    </ul>
                    //</div> ';
                return $button;
            })
            ->make();
        }
        return view('admin.report.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Report $report)
    {
        $report = Report::where('id', $report->id)->get();
        $reporter = Reporter::where('id', $report[0]->reporter_id)->get();

        if (!empty($report[0]->category_id)) {
            $category = Category::where('id', $report[0]->category_id)->get();
        } else {
            $category = [null];
        };

        return view('admin.report.show', [
            'report' => $report[0],
            'reporter' => $reporter[0],
            'category' => $category[0]
        ])->with('success', `Laporan dengan tiket {$report[0]->ticket_id} berhasil ditemukan.`);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Report $report)
    {
        $report = Report::where('id', $report->id)->get();
        $reporter = Reporter::where('id', $report[0]->reporter_id)->get();
        $categories = Category::all();

        $statuses = [
            'Pending',
            'Proses Administratif',
            'Proses Penanganan',
            'Selesai Ditangani',
            'Laporan Ditolak'
        ];

        return view('admin.report.form', [
            'report' => $report[0],
            'reporter' => $reporter[0],
            'statuses' => $statuses,
            'categories' => $categories
        ])->with('success', `Laporan dengan tiket {$report[0]->ticket_id} berhasil ditemukan.`);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Report $report)
    {
        $reporter = Reporter::where('id', $report->report_id);

        $report->clearMediaCollection('evidences');

        $reporter->delete();
        $report->delete();

        return redirect()->back();
    }

    public function addReportTracker(Request $request)
    {
        $reportTracker = ReportTracker::create([
            'report_id' => $request->report_id,
            'user_id' => $request->user_id,
            'status' => $request->status,
            'note' => $request->note,
        ]);

        $report = new Report();

        $report->where('id', $request->report_id)->update([
            'status' => $request->status,
            'category_id' => $request->category_id
        ]);

        return to_route('admin.report.index')->with('success', 'Laporan berhasil diperbarui.');
    }

    public function log(Report $report){
        return view('admin.report.log',[
            'logs' => Activity::where('subject_type', Report::class)->where('subject_id', $report->id)->latest()->get(),
            'report' => $report
        ]);
    }
}
