<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Category;
use App\Models\Reporter;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use App\Http\Requests\StoreReportRequest;
use App\Http\Requests\UpdateReportRequest;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('report.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('report.form', [
            'method' => 'POST',
            'reporter' => new Reporter(),
            'report' => new Report(),
            'route' => route('report.store')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'phone_number' => 'required',
            'identity_type' => 'required',
            'identity_number' => 'required',
            'pob' => 'required|string',
            'dob' => 'required',
            'address' => 'required',
            'title' => 'required',
            'description' => 'required',
            'evidences.*' => 'image|mimes:jpeg,png,jpg',
        ]);

        $reporter = Reporter::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'identity_type' => $request->identity_type,
            'identity_number' => $request->identity_number,
            'pob' => $request->pob,
            'dob' => $request->dob,
            'address' => $request->address,
        ]);

        $report = Report::create([
            'reporter_id' => $reporter->id,
            'ticket_id' => Str::random(16),
            'title' => $request->title,
            'description' => $request->description,
            'status' => 'Pending'
        ]);

        if ($request->hasFile('evidences')) {
            foreach ($request->file('evidences') as $evidence) {
                $report->addMedia($evidence)->usingFileName(Str::random(16).'')->toMediaCollection('evidences');
            }
        }

        return to_route('report.show', $report->ticket_id)->with('success', 'Laporan berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     */
    public function show($ticket_id)
    {
        $report = Report::where('ticket_id', $ticket_id)->get();
        $reporter = Reporter::where('id', $report[0]->reporter_id)->get();

        if (!empty($report[0]->category_id)) {
            $category = Category::where('id', $report[0]->category_id)->get();
        } else {
            $category = [null];
        };

        return view('report.show', [
            'report' => $report[0],
            'reporter' => $reporter[0],
            'category' => $category[0]
        ])->with('success', `Laporan dengan tiket {$report[0]->ticket_id} berhasil ditemukan.`);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateReportRequest $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Report $report)
    {
        //
    }

    public function search_ticket(Request $request)
    {
        $report = Report::where('ticket_id', $request->search_ticket)->first();

        if ($report) {
            return redirect()->route('report.show', $report->ticket_id)->with('success', 'Laporan berhasil ditemukan.');
        } else {
            return redirect()->back()->with('error', 'Laporan tidak ditemukan.');
        }
    }
}
