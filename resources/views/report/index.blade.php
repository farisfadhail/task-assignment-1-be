@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Halaman pengaduan - Cari laporan anda berdasarkan tiket laporan atau buat laporan baru
                    </div>
                    <div class="card-body">
                        <div class="accordion-item">
                            <div class="accordion-body">
                                <form action="{{ route('report.search') }}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="search_ticket" class="form-label">Masukkan nomor tiket laporan anda</label>
                                        <input type="text" name="search_ticket" id="search_ticket" class="form-control" placeholder="Cari Tiket..." value="">
                                    </div>

                                    <div class="d-flex justify-content-between">
                                        <a href="{{ route('report.create') }}" class="btn btn-outline-success">
                                            Buat laporan baru
                                        </a>
                                        <button type="submit" class="btn btn-primary">
                                            Cari laporan anda
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
