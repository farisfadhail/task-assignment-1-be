@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div>
                            Detail Laporan - {{ $report->ticket_id }}
                        </div>
                        <div>
                            <a href="{{ route('report.index') }}" class="btn btn-danger">Kembali ke pencarian</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="accordion-item">
                            <div class="accordion-body">
                                <p class="list-group-item">
                                    <strong>Nomor tiket:</strong>
                                    {{ $report->ticket_id }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Dibuat pada:</strong>
                                    {{ date('j F Y, H:i', strtotime($report->created_at)) }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Status:</strong>
                                    {{ $report->status }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Kategori:</strong>
                                    @if (is_null($category))
                                        Kategori belum ditentukan!
                                    @else
                                        {{ $category->name}}
                                    @endif
                                </p>
                                <p class="list-group-item">
                                    <strong>Nama Pelapor:</strong>
                                    {{ $reporter->name }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Email Pelapor:</strong>
                                    {{ $reporter->email }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Nomor HP Pelapor:</strong>
                                    {{ $reporter->phone_number }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Tipe Identitas Pelapor:</strong>
                                    {{ $reporter->identity_type }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Nomor Identitas Pelapor:</strong>
                                    {{ $reporter->identity_number }}
                                </p>
                                <p class="list-group-item">
                                    <strong>TTL Pelapor:</strong>
                                    {{ $reporter->pob.', '.date('j F Y', strtotime($reporter->dob)) }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Alamat Pelapor:</strong>
                                    {{ $reporter->address }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Judul Laporan:</strong>
                                    {{ $report->title }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Content:</strong>
                                    {{ $report->description }}
                                </p>
                                <p class="list-group-item mb-4">
                                    <strong>Bukti Laporan:</strong>
                                </p>
                                @foreach ($report->getMedia('evidences') as $evidence)
                                    <img src="{{ $evidence->getUrl() }}" width="200" class="me-4" alt="Bukti Laporan">
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
