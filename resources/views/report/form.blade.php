@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ $route }}" method="post" enctype="multipart/form-data">
                        @if ($method == 'PUT') @method('PUT') @endif
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Nama</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name',$reporter->name) }}">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email',$reporter->email) }}">
                        </div>
                        <div class="mb-3">
                            <label for="phone_number" class="form-label">Nomor HP</label>
                            <input type="text" name="phone_number" id="phone_number" class="form-control" value="{{ old('phone_number',$reporter->phone_number) }}">
                        </div>
                        <div class="mb-3">
                            <label for="identity_type" class="form-label">Tipe Identitas</label>
                            <select name="identity_type" id="identity_type" class="form-control">
                                <option value="">Choose type of identity</option>
                                <option value="" disabled>--------</option>
                                <option value="KTP">KTP</option>
                                <option value="SIM">SIM</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="identity_number" class="form-label">Nomor Identitas</label>
                            <input type="number" name="identity_number" id="identity_number" class="form-control" value="{{ old('identity_number',$reporter->identity_number) }}">
                        </div>
                        <div class="mb-3">
                            <label for="pob" class="form-label">Tempat Lahir</label>
                            <input type="text" name="pob" id="pob" class="form-control" value="{{ old('pob',$reporter->pob) }}">
                        </div>
                        <div class="mb-3">
                            <label for="dob" class="form-label">Tanggal Lahir</label>
                            <input type="date" name="dob" id="dob" class="form-control" value="{{ old('dob',$reporter->dob) }}">
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-label">Alamat</label>
                            <input type="text" name="address" id="address" class="form-control" value="{{ old('address',$reporter->address) }}">
                        </div>
                        <div class="mb-3">
                            <label for="title" class="form-label">Judul Laporan</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{ old('title',$report->title) }}">
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Deskripsi Laporan</label>
                            <textarea name="description" id="description" class="form-control">{{ old('description',$reporter->description) }}</textarea>
                        </div>
                        <div class="mb-3">
                            <label for="evidences" class="form-label">Bukti Laporan</label>
                            <input type="file" name="evidences[]" id="evidences" class="form-control" multiple>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            Buat Laporan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
