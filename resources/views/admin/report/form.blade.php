@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div>
                            Detail Laporan - {{ $report->ticket_id }}
                        </div>
                        <div>
                            <a href="{{ route('admin.report.index') }}" class="btn btn-danger">Kembali ke Dashboard</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="accordion-item">
                            <div class="accordion-body">
                                <form action="{{ route('admin.report.addReportTracker') }}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="status" class="form-label">Status Pengaduan</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Pilih status</option>
                                            <option value="" disabled>--------</option>
                                            @foreach ($statuses as $status)
                                                <option value="{{ $status }}" {{ $status == $report->status ? 'selected' : '' }}>{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="category_id" class="form-label">Kategori</label>
                                        <select name="category_id" id="category_id" class="form-control">
                                            <option value="">Pilih Kategori</option>
                                            <option value="" disabled>--------</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" {{ $report->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="note" class="form-label">Deskripsi</label>
                                        <textarea name="note" id="note" class="form-control">{{ old('note') }}</textarea>
                                    </div>
                                    <input type="hidden" name="report_id" id="report_id" class="form-control" value="{{ $report->id }}">
                                    <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ Auth::id() }}">
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn btn-primary">
                                            Ubah status
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <hr class="mx-4">
                    <div class="card-body">
                        <div class="accordion-item">
                            <div class="accordion-body">
                                <p class="list-group-item">
                                    <strong>Nomor tiket:</strong>
                                    {{ $report->ticket_id }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Dibuat pada:</strong>
                                    {{ date('j F Y, H:i', strtotime($report->created_at)) }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Status:</strong>
                                    {{ $report->status }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Nama Pelapor:</strong>
                                    {{ $reporter->name }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Email Pelapor:</strong>
                                    {{ $reporter->email }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Nomor HP Pelapor:</strong>
                                    {{ $reporter->phone_number }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Tipe Identitas Pelapor:</strong>
                                    {{ $reporter->identity_type }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Nomor Identitas Pelapor:</strong>
                                    {{ $reporter->identity_number }}
                                </p>
                                <p class="list-group-item">
                                    <strong>TTL Pelapor:</strong>
                                    {{ $reporter->pob.', '.date('j F Y', strtotime($reporter->dob)) }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Alamat Pelapor:</strong>
                                    {{ $reporter->address }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Judul Laporan:</strong>
                                    {{ $report->title }}
                                </p>
                                <p class="list-group-item">
                                    <strong>Content:</strong>
                                    {{ $report->description }}
                                </p>
                                <p class="list-group-item mb-4">
                                    <strong>Bukti Laporan:</strong>
                                </p>
                                @foreach ($report->getMedia('evidences') as $evidence)
                                    <img src="{{ $evidence->getUrl() }}" width="200" class="me-4" alt="Bukti Laporan">
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
