<?php

namespace Database\Seeders;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Date;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            [
                'name' => 'Infrastruktur',
                'slug' => Str::slug('Infrastruktur'),
                //'created_at' => Carbon::now()->timestamp,
                //'updated_at' => Carbon::now()->timestamp
            ],
            [
                'name' => 'Lingkungan',
                'slug' => Str::slug('Lingkungan'),
            ],
            [
                'name' => 'Layanan Publik',
                'slug' => Str::slug('Layanan Publik'),
            ],
            [
                'name' => 'Keamanan',
                'slug' => Str::slug('Keamanan'),
            ],
            [
                'name' => 'Kesehatan',
                'slug' => Str::slug('Kesehatan'),
            ],
            [
                'name' => 'Lain-lain',
                'slug' => Str::slug('Lain-lain'),
            ],
        ];

        Category::insert($categories);
    }
}
