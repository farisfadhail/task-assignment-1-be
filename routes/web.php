<?php

use App\Http\Controllers\Admin\ReportController as AdminReportController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::resource('report', ReportController::class)->except([
    'show',
]);
Route::get('report/{ticket_id}', [ReportController::class, 'show'])->name('report.show');
Route::post('report/search', [ReportController::class, 'search_ticket'])->name('report.search');

Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {
    Route::get('report/{report}/log', [AdminReportController::class, 'log'])->name('report.log');
    Route::post('report/add-report-tracker', [AdminReportController::class, 'addReportTracker'])->name('report.addReportTracker');
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::resource('report', AdminReportController::class);
});

